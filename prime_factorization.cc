#include <iostream>
#include <stdio.h>
using namespace std;

//Implement this function
string primeFactorization(unsigned int number);

int main(int argc, char** argv)
{
	if(argc<1) return -1;

	unsigned int number;
	float check;
	if(sscanf(argv[1],"%f",&check)==1)
	{
		if(check==(int)check)
		{	
			number=check;
			cout << primeFactorization(number) <<endl;
		}
	}

	return 0;
}

string primeFactorization(unsigned int number)
{
	int i;
	int j=0;
	int num; //number이 변하는걸 대신 할 수.
	string total;
	int count=0; //소인수분해되는 갯수만큼 배열의 길이를 만들기 위해 그 갯수를 알려주는 변수.
	int check=0;  //소인수 분해 되는 숫자들을 배열에 집어넣기 위한 변수.
	
	num=number;
	for(i=2;i<=num;i++)
	{
		if(num%i==0)
		{
			num=num/i;
			count+=1;
			i=1;
		}
	}
	num=number;

	int*arr=new int[count]; // 소인수 분해되는 숫자들이 들어가는 배열.
	
	for(i=2;i<=num;i++)
	{
		if(num%i==0)
		{
			num=num/i;
			arr[check]=i;
			check+=1;
			i=1;
		}
	}

	int*arr2=new int[count]; //소인수 분해된 숫자 별 갯수가 들어갈 배열.
	int*arr3=new int[count]; //소인수 분해된 숫자 종류들이 들어갈 배열.
	int check2=0; //소인수 분해 숫자별 갯수가 들어갈 공간과 숫자 종류가 들어갈 공간을 가리키는 변수.
	int count2; //같은 숫자의 갯수 나타내는 변수.

	for(i=0;i<count;i++)
	{
		arr2[i]=0;
	}
	
	for(i=0;i<count;i++)
	{
		arr3[i]=0;
	}

	while (j<count)
	{
		count2=0;
		for(i=j;i<count;i++)
		{
			if(arr[i]==arr[j])
				count2+=1;
		}
		arr2[check2]=count2;
		arr3[check2]=arr[j];
		check2+=1;
		j+=count2;
	}
	
	for(i=0;i<check2;i++)
	{
		if(arr3[i]>=10)
		{
			if(arr3[i]>=100)
			{
				total=total+((char)(48+arr3[i]/100));
				total=total+((char)(48+(arr3[i]%100)/10));
				total=total+((char)(48+((arr3[i]%100)%10)));
			}
			else
			{
				total=total+((char)(48+arr3[i]/10));
				total=total+((char)(48+arr3[i]%10));
			}
		}
		else
			total=total+((char)(48+arr3[i]));
	
		total=total+"^";
	
		if(arr2[i]>=10)
		{
			if(arr3[i]>=100)
			{
				total=total+((char)(48+arr2[i]/100));
				total=total+((char)(48+(arr2[i]%100)/10));
				total=total+((char)(48+((arr2[i]%100)%10)));
			}
			else
			{
				total=total+((char)(48+arr2[i]/10));
				total=total+((char)(48+arr2[i]%10));
			
			}
		}
		else
			total=total+((char)(48+arr2[i]));

		if(i!=check2-1)
			total=total+" X ";
	}

	delete[] arr;
	delete[] arr2;
	delete[] arr3;
	
	return total;
}
		

