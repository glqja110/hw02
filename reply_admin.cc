#include <iostream>
#include <string>
#include <stdio.h>
using namespace std;

const int NUM_OF_CHAT = 200;

int getChatCount(string *_chatList)      //_chatList라는 string 배열에 있는 글들의 갯수를 반환하는 함수.
{
	int i;
	for(i=0; i<NUM_OF_CHAT; ++i)
	{
		string s = _chatList[i];
		if(s.empty() == true) break;
	}
	return i;

}

void printChat(string *_chatList)  //_chatList를 출력하는 함수. (1 Hello World 처럼 표현하는 곳에 쓰일 함수임) _chatList만 이 함수에 잘 넣어주면 됨
{
	int count = getChatCount(_chatList);
	for(int i=0; i<count; ++i)
	{
		cout << i << " " << _chatList[i] <<endl;
	}
}

//Implement these functions
bool addChat(string *_chatList, string _chat); // returns true when adding chat is succeeded  
bool removeChat(string *_chatList, int _index); //returns true when removing chat is succeeded

//Implement commented (/* */) areas in main function
int main(void)
{
	string* chats = new string[NUM_OF_CHAT];    //chats 라는 200 크기의 string형 배을 만듦. 위 함수들의 _chatList가 될 변수.
	
	addChat(chats, "Hello, Reply Administrator!");   //addChat 함수 안에 pritChat을 구현해야할 것으로 보임.
	addChat(chats, "I will be a good programmer.");
	addChat(chats, "This class is awesome.");
	addChat(chats, "Professor Lim is wise.");
	addChat(chats, "Two TAs are kind and helpful.");
	addChat(chats, "I think male TA looks cool.");  //이렇게 5개가 이미 출력되어 있는 상태.

	while(true)  //false가 안나오는 이상 계속 작성하고 지우는 작업이 가능함.
	{
		string command;
		getline(cin, command);   //입력하는 것이 command 라는 이름으로 저장(?) 됨.
		string check; //#remove를 적었는 지 확인하는 변수.
		int i;
		for(i=0;i<7;i++)
		{
			check+=command[i];
		}
		if(command=="#quit") break;         //#quit을 입력하면 종료됨. 아마 addChat(#quit)을 하는데 이것이 false값 가지게 해야할것 같음.
			
		else if(check=="#remove")    //#remove가 작동되게 하는 구간.
		{
		
			int nnum; //범위 안에 있는 숫자를 가리키는 변수.
			int down; //제거할때 배열이 하나씩 옮겨지므로 그 옮겨진 부분을 생각하고 제거할수 있게 하는 변수.
			int i,a;
			string range;
			for(i=8;i<command.length();i++)
			{
				range+=command[i];
			}

			if(range.length()==1)  //숫자 하나만 입력 했을 경우.
			{
				nnum=((int)(range[0]-48));
				if(removeChat(chats, nnum)==true)
			 		printChat(chats);
			}
			else if(range.length()!=1)
			{
				if(range[1]==',')
				{
					if(range.length()>3)
					{
						if(range[3]!=',')	
							return true;
					}
					
					down=0;
					for(int i=0;i<(range.length()/2+1);i++)
					{
						nnum=((int)(range[2*i]-48));
						
						if(removeChat(chats, nnum-down)==true)
			 			{
							if(i==(range.length()/2))
								printChat(chats);
						}
						down++;
					}
					down=0;

				}					

				else if(range[1]=='-')
				{
					if(range.length()>3)
					{
						if(range[3]==',')
							return true;
						
					}
	
					down=0;
					int replace; //O-O형태에서 뒤에 O에들어가는 숫자를 나타내는 변수.
					replace=((int)(range[2]-48));
					
		
					if(replace>getChatCount(chats)-1||range.length()>3)
						replace=getChatCount(chats)-1;
					for(i=((int)(range[0]-48));i<=replace;i++)
					{
						nnum=i;	
						
						if(removeChat(chats, nnum-down)==true)
			 			{
							if(i==replace)
								printChat(chats);
						}
						down++;
					}
					down=0;
				}
			}	
						
		}
		else if(addChat(chats, command)) printChat(chats);
	}
	
	// delete chatting list
	delete [] chats;
	
	return 0;
}

bool addChat(string *_chatList, string _chat)  //chatList(배열)안에 _chat("Hello, Reply Administrator! 같은 것)을 넣어줘야함.
{
	int i;
	int num;
	
	if(_chat[0]=='#')
		return false;

	else
	{
		for(i=0; i<NUM_OF_CHAT; ++i)   //_chatList배열 몇번째에 넣을 것인가를 찾아내는 반복문.
		{
			string s = _chatList[i];
			if(s.empty() == true) 
			{
				num=i;
				break;
			}
		}	
	}
	
	_chatList[num]=_chat; //가장 비어있는 앞부분에 _char 넣어주기.
	return true;

}

bool removeChat(string *_chatList, int _index)
{
	int i;
	int num;
	
	for(i=0; i<NUM_OF_CHAT; ++i)   //_chatList배열이 몇개인지 확인해서 그 보다 많은 숫자를 적는 것을 방지하기 위함.
	{
		string s = _chatList[i];
		if(s.empty() == true) 
		{
			num=i;
			break;
		}
	}	
	if(_index>=num||_index<0)
		return false;
	else
	{
		if(_index==num-1)
			_chatList[num-1]="";
		else
		{
		
			for(i=_index;i<num-1;i++)
			{
				_chatList[i]=_chatList[i+1];
			}
			_chatList[num-1]="";
	
		}
		return true;
	}
}



