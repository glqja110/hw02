#include <stdio.h>
#include <iostream>
#include <string>
using namespace std;

//Implement this function.
string RadixNotation(unsigned int number, unsigned int radix);

int main(int argc, char** argv)  
{
	if (argc<2) return -1;  
	unsigned int radix;
	
	sscanf(argv[1],"%u",&radix);
	if(radix<2 || radix>36) return -1; 

	for(int i=2;i<argc;++i)
	{
		unsigned int number;
		sscanf(argv[i],"%u",&number);
		cout << RadixNotation(number, radix) << endl;
	}
	return 0;
}

string RadixNotation(unsigned int number, unsigned int radix)
{
	int check;
	int alpha;
	string total;
	int count=0;
	
	check=number;
	while(1)
	{
		if((check/radix)>=radix)
		{
			alpha=0;
			if((check%radix)>=10)
			{
				alpha=(check%radix)-10;
				total=((char)(97+alpha))+total;
			}
			else 
			{
				total=((char)(48+(check%radix)))+total;
			}
			check/=radix;
		}
		
		else
		{
			alpha=0;
			if((check%radix)>=10)
			{
				alpha=(check%radix)-10;
				total=((char)(97+alpha))+total;
			}
			else 
			{
				total=((char)(48+(check%radix)))+total;
			}

			alpha=0;
			if((check/radix)>=10)
			{
				alpha=(check/radix)-10;
				total=((char)(97+alpha))+total;
			}
			else
			{
				if(check/radix!=0)
					total=((char)(48+(check/radix)))+total;
			}
			break;
		}
		
	}
	
	
	return total;	
}

			
	
		
	
		
